<!DOCTYPE html>
<html lang="en">
  <head>
    <title>CodexArtist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/jquery-ui.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="assets/fonts/flaticon/flaticon.css">

    <link rel="stylesheet" href="assets/css/aos.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <style type="text/css">
      .btn-primary {
        background-color: orange !important;
        border-color: orange !important;
      }
      .ft-feature-1 .play{
       background:orange !important;
     }
     .site-section-heading.text-center::after{
      background:orange !important;
    }
    .site-navbar .site-navigation .site-menu .active > a{
      color: orange !important;
    }
    .site-navbar .site-navigation .site-menu .hover > a{
      color: orange !important;
    }
    .site-mobile-menu .site-nav-wrap li.active > a{
      color: orange !important;
    }
    .bg-primary{
     background-color: orange !important;
        border-color: orange !important;
    }
    a.bg-primary:hover, a.bg-primary:focus, button.bg-primary:hover, button.bg-primary:focus{
     background-color: gray !important;
        border-color: gray !important; 
    }
    .ft-feature-1 h3 .icon{
      color: orange;
    }
    
  
  </style>
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    <div class="border-bottom top-bar py-2">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p class="mb-0">
             
              <span><strong>Email:</strong> <a href="#" style="color: orange !important;">codexartist2019@gmail.com</a></span>
            </p>
          </div>
          <div class="col-md-6">
            <ul class="social-media">
              <li><a style="color: orange !important;" href="#" class="p-2"><span class="icon-facebook"></span></a></li>
              <li><a style="color: orange !important;" href="#" class="p-2"><span class="icon-twitter"></span></a></li>
              <li><a style="color: orange !important;" href="#" class="p-2"><span class="icon-instagram"></span></a></li>
              <li><a style="color: orange !important;" href="#" class="p-2"><span class="icon-linkedin"></span></a></li>
            </ul>
          </div>
        </div>
      </div> 
    </div>
    <header class="site-navbar py-4 bg-white" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-11 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.php" class="text-black h2 mb-0">CodexArtist</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="work.php">Work</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="contact.php">Contact</a></li>
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>